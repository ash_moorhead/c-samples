﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Threading;
using System.Web.Script.Serialization;




namespace Stock_Analyser
{
    public partial class Main : Form
    {

        private List<string> TICKERS = new List<string>();
        private string Stat;
        private List<Classes.Stock> stockList = new List<Classes.Stock>();
        private Task task;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private BindingSource bindingSource1 = new BindingSource();

        public Main()
        {
            InitializeComponent();

        }

        private void downloadButton_Click(object sender, EventArgs e)
        {
            if (downloadButton.Text == "Start Download")
            {
                SetVisibility(true, progressBar1);
                progressBar1.Style = ProgressBarStyle.Marquee;
                progressBar1.MarqueeAnimationSpeed = 20;
                downloadButton.Text = "Stop Download";

                if (task == null)
                {
                    // Run the download on a background thread
                    Action download = new Action(Download_Detailed_Data);
                    task = Task.Factory.StartNew(download, tokenSource.Token);
                    task.ContinueWith(cont => 
                        {
                            Populate_DataGridView();
                            SetVisibility(false, progressBar1);
                        });
                }
            }
            else
            {
                if (task != null)
                {
                    tokenSource.Cancel();
                }
                SetVisibility(false, progressBar1);
                downloadButton.Text = "Start Download";
            }

        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {

                #region load the S&P500 companies
                var SP500 = @"..\..\data\sp500CompanyList.csv";
                //TODO add in the dow jones and make it work for a combobox of indexes worldwide
                //var DJI = @"..\..\data\djiCompanyList.csv";
                if (File.Exists(SP500))
                {
                    // we use a "using" block so that C# will automatically clean up all unused resources
                    using (var reader = new StreamReader(SP500))
                    {
                        while (!reader.EndOfStream)
                        {
                            var csvLine = reader.ReadLine();
                            var firstCommaIndex = csvLine.IndexOf(",");
                            var stock = csvLine.Substring(0, firstCommaIndex);

                            // In case there are symbols in the stock names that would not be recognized by Yahoo API
                            if (!TICKERS.Contains(stock) && !stock.Contains('/'))
                            {
                                TICKERS.Add(stock);
                            }
                        }
                    }

                }
                else
                {
                    label2.Visible = true;
                }
                #endregion                
                SetVisibility(false, progressBar1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}", ex), "Error!");
            }

        }

        private void Download_Detailed_Data()
        {
            while (!tokenSource.IsCancellationRequested)
            {
                string prototypeURL = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20(%22{0}%22)&format=json&env=http://datatables.org/alltables.env";

                try
                {

                string[] jsonArray = new string[5];

                for (int j = 0; j < 5; j++)
                {
                    string commaTickers = string.Join(",", TICKERS.GetRange(j * 100, 100));


                    Uri url = new Uri(string.Format(prototypeURL, commaTickers));

                    // download json formatted Data
                    IWebProxy defaultWebProxy = WebRequest.DefaultWebProxy;
                    defaultWebProxy.Credentials = CredentialCache.DefaultCredentials;
                    WebClient wc = new WebClient
                    {
                        Proxy = defaultWebProxy
                    };
                    wc.Encoding = Encoding.UTF8;

                    jsonArray[j] = wc.DownloadString(url);

                    //And parse
                    var jss = new JavaScriptSerializer();
                    var dict = jss.Deserialize<Dictionary<string, dynamic>>(jsonArray[j]);


                    for (int i = 0; i < 100; i++)
                    {
                        Classes.Stock stock = new Classes.Stock(TICKERS[i + (j * 100)]);

                        //store variables in class
                        stock.Ask = dict["query"]["results"]["quote"][i]["Ask"];
                        stock.Bid = dict["query"]["results"]["quote"][i]["Bid"];
                        stock.BookValue = dict["query"]["results"]["quote"][i]["BookValue"];
                        stock.ChangeInPercent = dict["query"]["results"]["quote"][i]["ChangeinPercent"];
                        stock.DaysRange = dict["query"]["results"]["quote"][i]["DaysRange"];
                        stock.DividendYield = dict["query"]["results"]["quote"][i]["DividendYield"];
                        stock.EPS = dict["query"]["results"]["quote"][i]["EarningsShare"];
                        stock.EPSEstimateNextQuarter = dict["query"]["results"]["quote"][i]["EPSEstimateNextQuarter"];
                        stock.EPSEstimateNextYear = dict["query"]["results"]["quote"][i]["EPSEstimateNextYear"];
                        stock.MarketCap = dict["query"]["results"]["quote"][i]["MarketCapitalization"];
                        stock.OneYearTargetPrice = dict["query"]["results"]["quote"][i]["OneyrTargetPrice"];
                        stock.PERatio = dict["query"]["results"]["quote"][i]["PERatio"];
                        stock.PriceToBook = dict["query"]["results"]["quote"][i]["PriceBook"];
                        stock.PriceToSales = dict["query"]["results"]["quote"][i]["PriceSales"];
                        stock.ShortRatio = dict["query"]["results"]["quote"][i]["ShortRatio"];
                        stock.YearRange = dict["query"]["results"]["quote"][i]["YearRange"];

                        //Add to list of stocks
                        stockList.Add(stock);

                    }

                }
                if (stockList.Count >= 500)
                {
                    tokenSource.Cancel();
                }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("{0}", ex), "Error!");
                }
            }

        }

        private void Populate_DataGridView()
        {
            // Initialise DataTable with columns
            DataTable AllInfo = GetTable();

            // Add Stock data to dataTable
            foreach (Classes.Stock stock in stockList)
            {
                AllInfo.Rows.Add(stock.Ticker, stock.Ask, stock.Bid, stock.BookValue,
                    stock.ChangeInPercent, stock.DaysRange, stock.DividendYield,
                    stock.EPS, stock.EPSEstimateNextQuarter, stock.EPSEstimateNextYear,
                    stock.MarketCap, stock.OneYearTargetPrice, stock.PERatio,
                    stock.PriceToBook, stock.PriceToSales, stock.ShortRatio, stock.YearRange);
            }

            //Bind to the GridView
            SetDataSource(bindingSource1, dataGridView1);
            this.Invoke(new MethodInvoker(delegate { bindingSource1.DataSource = AllInfo; }));
            this.Invoke(new MethodInvoker(delegate
            {
                dataGridView1.AutoResizeColumns(
                    DataGridViewAutoSizeColumnsMode.AllCells);
            })); 

        }

        private DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("Ticker", typeof(string));
            table.Columns.Add("Ask", typeof(string));
            table.Columns.Add("Bid", typeof(string));
            table.Columns.Add("BookValue", typeof(string));
            table.Columns.Add("ChangeinPercent", typeof(string));
            table.Columns.Add("DaysRange", typeof(string));
            table.Columns.Add("DividendYield", typeof(string));
            table.Columns.Add("EarningsShare", typeof(string));
            table.Columns.Add("EPSEstimateNextQuarter", typeof(string));
            table.Columns.Add("EPSEstimateNextYear", typeof(string));
            table.Columns.Add("MarketCapitalization", typeof(string));
            table.Columns.Add("OneyrTargetPrice", typeof(string));
            table.Columns.Add("PERatio", typeof(string));
            table.Columns.Add("PriceBook", typeof(string));
            table.Columns.Add("PriceSales", typeof(string));
            table.Columns.Add("ShortRatio", typeof(string));
            table.Columns.Add("YearRange", typeof(string));

            return table;
        }

        private void SetDataSource(BindingSource dataSource, DataGridView DGV)
        {
            if (DGV.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate { DGV.DataSource = dataSource; }));
            }
            else
            {
                DGV.DataSource = dataSource;
            }
        }

        #region Visibility Control
        private void SetVisibility(bool vis, ProgressBar progress)
        {
            if (progress.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate { progress.Visible = vis; }));
            }
            else
            {
                progress.Visible = vis;
            }
        }
        private void SetVisibility(bool vis, DataGridView progress)
        {
            if (progress.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate { progress.Visible = vis; }));
            }
            else
            {
                progress.Visible = vis;
            }
        }
        private void SetVisibility(bool vis, Button progress)
        {
            if (progress.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate { progress.Visible = vis; }));
            }
            else
            {
                progress.Visible = vis;
            }
        }
        #endregion

    }
}
