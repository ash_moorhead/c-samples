﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Stock_Analyser.Classes
{
    class Stock
    {

        public Stock(string vTicker)
        {
            Ticker = vTicker;
        }

        public string Ticker { get; set; }

        public string Ask { get; set; }

        public string Bid { get; set; }

        public string BookValue { get; set; }

        public string ChangeInPercent { get; set; }

        public string EPS { get; set; }

        public string EPSEstimateNextYear { get; set; }

        public string EPSEstimateNextQuarter { get; set; }

        public string YearRange { get; set; }

        public string MarketCap { get; set; }

        public string DaysRange { get; set; }

        public string PriceToSales { get; set; }

        public string PriceToBook { get; set; }

        public string PERatio { get; set; }

        public string ShortRatio { get; set; }

        public string OneYearTargetPrice { get; set; }

        public string DividendYield { get; set; }



    }
}
