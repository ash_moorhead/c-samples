﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;

namespace GraphingTool
{
    public partial class Main : Form
    {

        string varName,
            file;
        int[] readTime = new int[3];
        DateTime time;
        DateTime startTime;
        DateTime stopTime;
        Dictionary<DateTime, double> data = new Dictionary<DateTime, double>();
        bool First = true;
        int i = 1;

        public Main()
        {
            InitializeComponent();

        }

        private void ReadFile()
        {
            try
            {

                string line;
                int counter = 0,
                    varIndex = 0;

                // Read in the values of the variables between the input timestamps and store in an array
                System.IO.StreamReader stream = new System.IO.StreamReader(file);

                //read line
                while ((line = stream.ReadLine()) != null)
                {
                    //ignore first four lines
                    if (counter < 4)
                    {
                        if (counter == 3)
                        {

                            // get the index of the variable
                            string[] varNames = line.Split('\t');
                            for (int i = 0; i < varNames.Length; i++)
                            {
                                if (varNames[i] == varName)
                                {
                                    varIndex = i;
                                    break;
                                }
                            }

                        }

                        counter++;
                        continue;
                    }

                    //save timestamp in ReadTime
                    string[] parts = line.Split(':');
                    for (int i = 0; i < 3; i++)
                    {
                        if (!int.TryParse(parts[i], out readTime[i]))
                        {
                            throw new Exception("Parsing time interval to integers failed!");
                        }
                    }

                    TimeSpan ts = new TimeSpan(readTime[0], readTime[1], readTime[2]);
                    time = time.Date + ts;

                    //check if passed start time
                    if (DateTime.Compare(startTime, time) < 0)
                    {

                        // read data from correct column and save to data dictionary      

                        if (varIndex != 0)
                        {
                            double value;
                            string[] vars = parts[3].Split('\t');
                            double.TryParse(vars[varIndex], out value);
                            data.Add(time, value);
                        }
                        else
                        {
                            throw new Exception("Variable name does not exist!");
                        }


                        //check for stop
                        if (DateTime.Compare(stopTime, time) < 0)
                        {
                            return;
                        }
                    }

                    //increment
                    counter++;
                }
                if (data.Count == 0)
                {
                    throw new Exception("No data within that interval for the selected variable");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("{0}", e), "Error!");
            }

        }

        private void plot(bool First)
        {
            try
            {
                
                if (!First)
                {
                    // Add a new series
                    Series series = Chart1.Series.Add("Series" + i);
                    series.ChartType = SeriesChartType.Spline;

                    // Add data to the new series
                    foreach (KeyValuePair<DateTime, double> entry in data)
                    {
                        this.Chart1.Series["Series" + i].Points.AddXY(entry.Key.ToString("HH:mm:ss"), entry.Value);
                    }

                    // change label 
                    this.plotTitle.Text = this.plotTitle.Text + ", " + varName;
                    i++;
                }
                else
                {
                    //add data to first series
                    foreach (KeyValuePair<DateTime, double> entry in data)
                    {
                        this.Chart1.Series["Series0"].Points.AddXY(entry.Key.ToString("HH:mm:ss"), entry.Value);
                    }

                    this.plotTitle.Text = this.plotTitle.Text + varName;
                }


                
            }
            catch (Exception e3)
            {
                MessageBox.Show(String.Format("{0}", e3), "Error!");
            }

        }

        private void PlotButton_click(object sender, EventArgs e)
        {

            int[] start = new int[3];
            int[] stop = new int[3];

            try
            {

                varName = this.variableComboBox.Text;

                file = this.filenameText.Text;


                //read in the time interval
                int.TryParse(this.H1.Text, out start[0]);
                int.TryParse(this.M1.Text, out start[1]);
                int.TryParse(this.S1.Text, out start[2]);
                int.TryParse(this.H2.Text, out stop[0]);
                int.TryParse(this.M2.Text, out stop[1]);
                int.TryParse(this.S2.Text, out stop[2]);

                TimeSpan begin = new TimeSpan(start[0], start[1], start[2]);
                startTime = startTime.Date + begin;
                TimeSpan end = new TimeSpan(stop[0], stop[1], stop[2]);
                stopTime = stopTime.Date + end;

                //check if interval is valid (i.e H2M2S2 > H1M1S1)
                if (DateTime.Compare(startTime, stopTime) > 0)
                {
                    throw new Exception("Invalid interval!");
                }

                ReadFile();
                plot(First);
                First = false;
                data.Clear();

            }
            catch (Exception e1)
            {
                MessageBox.Show(String.Format("{0}", e1), "Error!");
            }

        }

        private string LoadNewFile()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                System.Windows.Forms.DialogResult dr = ofd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    return ofd.FileName;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e2)
            {
                MessageBox.Show(String.Format("{0}", e2), "Error!");
                return null;
            }

        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            // load file from dialog
            file = LoadNewFile();

            if (file != null)
            {
                this.filenameText.Text = file;
            }
        }

        private void clearPlot_Click(object sender, EventArgs e)
        {
            Chart1.Series.Clear();
            this.plotTitle.Text = "";
            // Add a base series
            Series series = Chart1.Series.Add("Series0");
            series.ChartType = SeriesChartType.Spline;
            First = true;
        }

        private void filenameText_TextChanged(object sender, EventArgs e)
        {
            try
            {
                variableComboBox.Items.Clear();

                string line;
                int counter = 0;
                string[] varNames = new string[10];
                string[] values = new string[10];
                int[] begin = new int[3];
                int[] end = new int[3];
                DateTime beginTime = new DateTime();
                DateTime endTime = new DateTime();

                #region read in the times and variable names

                // Read in the variable names 
                System.IO.StreamReader stream = new System.IO.StreamReader(file);


                //read line        
                while ((line = stream.ReadLine()) != null)
                {
                    // Act on the 3rd line
                    if (counter == 3)
                    {
                        // get the index of the variable
                        varNames = line.Split('\t');
                    }
                    if (counter == 4)
                    {
                        values = line.Split(':');
                        for (int i = 0; i < 3; i++)
                        {
                            if (!int.TryParse(values[i], out begin[i]))
                            {
                                throw new Exception("Parsing time interval to integers failed!");
                            }
                        }

                        TimeSpan ts1 = new TimeSpan(begin[0], begin[1], begin[2]);
                        beginTime = beginTime.Date + ts1;

                        break;
                    }
                    counter++;
                }

                line = File.ReadLines(file).Last();
                values = line.Split(':');
                        for (int i = 0; i < 3; i++)
                        {
                            if (!int.TryParse(values[i], out end[i]))
                            {
                                throw new Exception("Parsing time interval to integers failed!");
                            }
                        }

                        TimeSpan ts2 = new TimeSpan(end[0], end[1], end[2]);
                        endTime = endTime.Date + ts2;

                #endregion
                //Add to drop down menu
                for (int i = 1; i < varNames.Length; i++)
                {
                    variableComboBox.Items.Add(varNames[i]);
                }

                //Set label
                timeIntervalHint.Text = string.Format("File contains data from {0} to {1:hh:mm:ss}.", beginTime.ToString("HH:mm:ss"), 
                    endTime.ToString("HH:mm:ss"));

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}", ex), "Error!");
            }

        }

    }
}
