﻿namespace GraphingTool
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.filenameText = new System.Windows.Forms.TextBox();
            this.plotButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.H2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label4 = new System.Windows.Forms.Label();
            this.plotTitle = new System.Windows.Forms.Label();
            this.M2 = new System.Windows.Forms.TextBox();
            this.S2 = new System.Windows.Forms.TextBox();
            this.hr = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.S1 = new System.Windows.Forms.TextBox();
            this.M1 = new System.Windows.Forms.TextBox();
            this.H1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.clearPlot = new System.Windows.Forms.Button();
            this.variableComboBox = new System.Windows.Forms.ComboBox();
            this.timeIntervalHint = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // filenameText
            // 
            this.filenameText.AcceptsReturn = true;
            this.filenameText.AllowDrop = true;
            this.filenameText.Location = new System.Drawing.Point(42, 41);
            this.filenameText.Name = "filenameText";
            this.filenameText.ShortcutsEnabled = false;
            this.filenameText.Size = new System.Drawing.Size(120, 20);
            this.filenameText.TabIndex = 0;
            this.filenameText.Text = "Filename";
            this.filenameText.TextChanged += new System.EventHandler(this.filenameText_TextChanged);
            // 
            // plotButton
            // 
            this.plotButton.Location = new System.Drawing.Point(671, 40);
            this.plotButton.Name = "plotButton";
            this.plotButton.Size = new System.Drawing.Size(85, 21);
            this.plotButton.TabIndex = 9;
            this.plotButton.Text = "Plot";
            this.plotButton.UseVisualStyleBackColor = true;
            this.plotButton.Click += new System.EventHandler(this.PlotButton_click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name of File to Plot";
            // 
            // H2
            // 
            this.H2.Location = new System.Drawing.Point(400, 41);
            this.H2.MaxLength = 2;
            this.H2.Name = "H2";
            this.H2.Size = new System.Drawing.Size(21, 20);
            this.H2.TabIndex = 5;
            this.H2.Text = "hh";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(370, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "to";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(276, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Time Interval";
            // 
            // Chart1
            // 
            this.Chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.AxisX.MajorGrid.Interval = 0D;
            chartArea1.Name = "ChartArea1";
            this.Chart1.ChartAreas.Add(chartArea1);
            this.Chart1.Location = new System.Drawing.Point(43, 146);
            this.Chart1.Name = "Chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Name = "Series0";
            this.Chart1.Series.Add(series1);
            this.Chart1.Size = new System.Drawing.Size(745, 339);
            this.Chart1.TabIndex = 20;
            this.Chart1.TabStop = false;
            this.Chart1.Text = "chart1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(512, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Variable Name";
            // 
            // plotTitle
            // 
            this.plotTitle.AutoSize = true;
            this.plotTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plotTitle.Location = new System.Drawing.Point(57, 107);
            this.plotTitle.Name = "plotTitle";
            this.plotTitle.Size = new System.Drawing.Size(0, 26);
            this.plotTitle.TabIndex = 10;
            // 
            // M2
            // 
            this.M2.Location = new System.Drawing.Point(427, 41);
            this.M2.MaxLength = 2;
            this.M2.Name = "M2";
            this.M2.Size = new System.Drawing.Size(21, 20);
            this.M2.TabIndex = 6;
            this.M2.Text = "mm";
            // 
            // S2
            // 
            this.S2.Location = new System.Drawing.Point(454, 41);
            this.S2.MaxLength = 2;
            this.S2.Name = "S2";
            this.S2.Size = new System.Drawing.Size(21, 20);
            this.S2.TabIndex = 7;
            this.S2.Text = "ss";
            // 
            // hr
            // 
            this.hr.AutoSize = true;
            this.hr.BackColor = System.Drawing.Color.Transparent;
            this.hr.Location = new System.Drawing.Point(419, 45);
            this.hr.Name = "hr";
            this.hr.Size = new System.Drawing.Size(10, 13);
            this.hr.TabIndex = 13;
            this.hr.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(446, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = ":";
            // 
            // S1
            // 
            this.S1.Location = new System.Drawing.Point(333, 41);
            this.S1.MaxLength = 2;
            this.S1.Name = "S1";
            this.S1.Size = new System.Drawing.Size(21, 20);
            this.S1.TabIndex = 4;
            this.S1.Text = "ss";
            // 
            // M1
            // 
            this.M1.Location = new System.Drawing.Point(306, 41);
            this.M1.MaxLength = 2;
            this.M1.Name = "M1";
            this.M1.Size = new System.Drawing.Size(21, 20);
            this.M1.TabIndex = 3;
            this.M1.Text = "mm";
            // 
            // H1
            // 
            this.H1.Location = new System.Drawing.Point(279, 41);
            this.H1.MaxLength = 2;
            this.H1.Name = "H1";
            this.H1.Size = new System.Drawing.Size(21, 20);
            this.H1.TabIndex = 2;
            this.H1.Text = "hh";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(298, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(325, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = ":";
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(168, 41);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(69, 20);
            this.BrowseButton.TabIndex = 1;
            this.BrowseButton.Text = "Browse";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // clearPlot
            // 
            this.clearPlot.Location = new System.Drawing.Point(671, 68);
            this.clearPlot.Name = "clearPlot";
            this.clearPlot.Size = new System.Drawing.Size(85, 22);
            this.clearPlot.TabIndex = 21;
            this.clearPlot.Text = "Clear Chart";
            this.clearPlot.UseVisualStyleBackColor = true;
            this.clearPlot.Click += new System.EventHandler(this.clearPlot_Click);
            // 
            // variableComboBox
            // 
            this.variableComboBox.FormattingEnabled = true;
            this.variableComboBox.Location = new System.Drawing.Point(515, 42);
            this.variableComboBox.MaxDropDownItems = 20;
            this.variableComboBox.Name = "variableComboBox";
            this.variableComboBox.Size = new System.Drawing.Size(139, 21);
            this.variableComboBox.TabIndex = 8;
            // 
            // timeIntervalHint
            // 
            this.timeIntervalHint.AutoSize = true;
            this.timeIntervalHint.Location = new System.Drawing.Point(284, 65);
            this.timeIntervalHint.Name = "timeIntervalHint";
            this.timeIntervalHint.Size = new System.Drawing.Size(0, 13);
            this.timeIntervalHint.TabIndex = 22;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 511);
            this.Controls.Add(this.timeIntervalHint);
            this.Controls.Add(this.variableComboBox);
            this.Controls.Add(this.clearPlot);
            this.Controls.Add(this.BrowseButton);
            this.Controls.Add(this.S1);
            this.Controls.Add(this.M1);
            this.Controls.Add(this.H1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.S2);
            this.Controls.Add(this.M2);
            this.Controls.Add(this.plotTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Chart1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.H2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.plotButton);
            this.Controls.Add(this.filenameText);
            this.Controls.Add(this.hr);
            this.Controls.Add(this.label5);
            this.Name = "Main";
            this.Text = "GraphTool";
            ((System.ComponentModel.ISupportInitialize)(this.Chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox filenameText;
        private System.Windows.Forms.Button plotButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox H2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label plotTitle;
        private System.Windows.Forms.TextBox M2;
        private System.Windows.Forms.TextBox S2;
        private System.Windows.Forms.Label hr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox S1;
        private System.Windows.Forms.TextBox M1;
        private System.Windows.Forms.TextBox H1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.Button clearPlot;
        private System.Windows.Forms.ComboBox variableComboBox;
        private System.Windows.Forms.Label timeIntervalHint;
    }
}

